import 'package:flutter/material.dart';
import './widgets/students.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            title: Text("RecyclerApp",),
            backgroundColor: Colors.green,
          ),
          body: Students(),
      ),
    );
  }
}
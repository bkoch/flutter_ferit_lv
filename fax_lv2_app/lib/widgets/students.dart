import 'package:flutter/material.dart';
import './addStudent.dart';
import './studentList.dart';

class Students extends StatefulWidget {
  @override
  _StudentsState createState() => _StudentsState();
}

class _StudentsState extends State<Students> {

  List<String> studentNames=[
    'Marko','Ivan','Marin','Fran'
  ];

  void _addStudent(String name){
    setState(() {
      studentNames.add(name);
    });
  }

  void _removeStudent(int index){
    setState(() {
      studentNames.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
     return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          StudentList(studentNames,_removeStudent),
          AddStudent(_addStudent),
        ],
      );
  }
}
import 'package:flutter/material.dart';

class StudentList extends StatelessWidget {

  final List<String> studentNames;
  final Function removeStudent;

  StudentList(this.studentNames,this.removeStudent);

  @override
  Widget build(BuildContext context) {
    return Container(
      height:520,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.green,
          width:2
        )
      ),
      child:ListView.builder(
        itemCount: studentNames.length,
        itemBuilder: (ctx,index){
          return Card(
            elevation: 7,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(studentNames[index]),
                ),
                IconButton(
                  icon: Icon(Icons.remove_circle),
                  color: Colors.red,
                  onPressed: ()=>removeStudent(index),
                )
              ],
            ),
          );
        },
        
      )
    );
  }
}
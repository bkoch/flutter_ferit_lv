import 'package:flutter/material.dart';

class AddStudent extends StatelessWidget {
  
  Function addStudent;
  final TextEditingController nameController=TextEditingController();

  AddStudent(this.addStudent);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        margin: EdgeInsets.all(5),
        child: Row(
          children: <Widget>[
            Container(
              width: 200,
              child: TextField(
                controller: nameController,
                decoration: InputDecoration(
                  labelText: 'Ime'
                ),
              ),
            ),
            Expanded(
              child: RaisedButton(
                elevation: 7,
                child:Text("DODAJ"),
                onPressed: () => {
                  if(nameController.text.isNotEmpty){
                    addStudent(nameController.text),
                    nameController.clear()
                  }
                }
              ),
            )
          ],
        ),
      ),
    );
  }
}
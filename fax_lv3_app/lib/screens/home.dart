import 'package:flutter/material.dart';
import './home_fragments/tab1.dart';
import './home_fragments/tab2.dart';
import './home_fragments/tab3.dart';
import './home_fragments/tab4.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin{//explain

  TabController _tabController;
  String submitedText='';
  TextEditingController _textEditingController=TextEditingController();

  @override
  void initState(){
    super.initState();
    _tabController=new TabController(length: 4,vsync: this);//explain
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('LV3'),
        backgroundColor: Colors.green,
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Color(0xffb84476),
          tabs: <Widget>[
            Tab(text: "#1"),
            Tab(text: "#2"),
            Tab(text: "#3"),
            Tab(text: "#4")
          ],
        )
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          tab1(()=> _buttonFunction(),_textEditingController),
          tab2(submitedText),
          tab3(),
          tab4(true),
        ],
      ),
      
    );
  }

  void _buttonFunction(){
    setState(() {
      submitedText=_textEditingController.text;
      _textEditingController.clear();
      _tabController.animateTo(1);//EXPLAIN
    });
  }
}
import 'package:flutter/material.dart';

class tab4 extends StatelessWidget {

  bool isImage;

  tab4(this.isImage);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: isImage
            ? Image.asset(
                './images/image.jpg',
                height: 250,
                width: 250,
                fit: BoxFit.fill,
              )
            : Text("Hello World!"),
      ),

    );
  }
}
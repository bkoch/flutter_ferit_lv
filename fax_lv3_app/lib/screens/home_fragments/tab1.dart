import 'package:flutter/material.dart';

class tab1 extends StatelessWidget {

  Function buttonFunction;
  TextEditingController textEditingController;

  tab1(this.buttonFunction,this.textEditingController);

  @override
  Widget build(BuildContext context) {
    return Container(
      child:Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child:Container(
                child:TextField(
                  controller: textEditingController,
                  
                )
              )
            ),
            FlatButton(
              onPressed: buttonFunction,
              color: Colors.grey,
              child:Text('SUBMIT'),
            )
          ],
        ),
      )
    );
  }
}
import 'package:flutter/material.dart';
import '../models/item.dart';

class ItemCard extends StatelessWidget {
  Item item;
  ItemCard(this.item);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      margin: const EdgeInsets.all(8),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              FadeInImage.assetNetwork(
                placeholder: 'assets/images/placeholder.jpg',
                image: item.image_link,
                height: 80,
                width: 80,
              ),
              Container(
                width: MediaQuery.of(context).size.width - 96,
                height: 68,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Text("Name: ${item.name}"),
                    ),
                    Text("Price: ${item.price}"),
                    Text("Rating: ${item.rating}"),
                  ],
                ),
              )
            ],
          ),
          Expanded(
            child: Text(
              item.description,
              textAlign: TextAlign.justify,
            ),
          ),
        ],
      ),
    );
  }
}

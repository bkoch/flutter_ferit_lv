class Item {
  final String name;
  final double price;
  final double rating;
  final String image_link;
  final String description;

  Item({
    this.name,
    this.price,
    this.rating,
    this.image_link,
    this.description
  });

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      name: json["name"],
      price: double.tryParse(json["price"]),
      rating: json["rating"],
      image_link: json["image_link"],
      description: json["description"],
    );
  }
}

import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/item.dart';
import '../widgets/item_cart.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController textEditingController=TextEditingController();
  Widget result_list=Container();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('fax_lv4_app'),
      ),
      body: Container(
        child:Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: textEditingController,
                  ),
                ),
                FlatButton(
                  color: Colors.blue,
                  onPressed: () {
                    setState(() {
                      result_list = FutureBuilder(
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.waiting)
                            return Center(child: CircularProgressIndicator());
                          else if (snapshot.data == null)
                            return Container();
                          else if (snapshot.data.length == 0)
                            return Container(
                                child: Center(child: Text("None")));
                          return _buildList(snapshot.data);
                        },
                        future: _getItems(textEditingController.text),
                      );
                      textEditingController.clear();
                    });
                  },
                  child: Text("SEARCH"),
                )
              ],
            ),
            Expanded(
              child: result_list,
            )
          ],
        )
      ),
    );
  }

  Future<List<Item>> _getItems(String searchTerm) async {
    final url ="http://makeup-api.herokuapp.com/api/v1/products.json?brand=$searchTerm";

    final response = await http.get(url);

    List<Item> result = [];
    if (response.statusCode == 200) {
      final parsedJson = json.decode(response.body);
      for (int i = 0; i < (parsedJson as List).length; i++) {
        result.add(Item.fromJson(parsedJson[i]));
      }
      return result;
    }

    return null;
  }

  Widget _buildList(List<Item> items) => ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return ItemCard(items[index]);
        },
      );
}



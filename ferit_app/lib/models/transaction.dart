import 'dart:async';
import 'dart:io';

class Transaction{
  final String id;
  final String title;
  final double amount;
  final DateTime date;
  final File receiptImagePath;

  Transaction({this.id,this.title,this.amount,this.date,this.receiptImagePath});
}

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Person extends StatelessWidget{

  String imageLocation;
  String name;
  String yearOfBirth;
  String description;

  Person( this.imageLocation, this.name, this.yearOfBirth, this.description);

  void changeDesc(String text){
    this.description=text;
  }

  String get getImageLocation => imageLocation;
  String get getName => name;
  String get getYearOfBirth => yearOfBirth;

  @override
  Widget build(BuildContext context){
    return Container(
      width:double.infinity,
      height: 100,
      margin:EdgeInsets.all(10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Image.asset(imageLocation,alignment: Alignment.bottomLeft,),
            flex:1
            
          ),
          Expanded(
            flex: 2,
            child:Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  name,
                  style: TextStyle(fontSize: 28),
                  textAlign: TextAlign.center
                  ),
                Text(
                  yearOfBirth,
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.left),
                Text(
                  description,
                  textAlign: TextAlign.left,)
              ],
            )
          ) 
        ],
      )
    );
  }
}
import 'package:flutter/material.dart';
import './person.dart';
import 'dart:math';

void main() =>runApp(new MaterialApp(home: new MyApp()));// runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.

  TextEditingController myController=TextEditingController();
  int radioValue=2;
  int groupValue=-1;
  List<Person> personWidgets = [];

  void addPerson(Person person) {
    setState(() {
      personWidgets.add(person);
    });
  }

  void changeRadioValue(int value){
    setState(() {
      radioValue=value;
      groupValue=value;
    });
  }

  void changeDescription(int radioValue,String text){
    setState(() {
      Person editedPerson = Person(personWidgets[radioValue].getImageLocation, personWidgets[radioValue].getName, personWidgets[radioValue].getYearOfBirth, text);
      personWidgets.removeAt(radioValue);
      personWidgets.insert(radioValue, editedPerson);
      myController.clear();
    });
  }

  

  @override
  void initState() {
    super.initState();
    personWidgets.add(Person(
        'assets/images/nikola_tesla.jpg', 'Nikola Tesla', '1856-1943', 'Inventor Nikola Tesla contributed to the development of the alter...'));
    personWidgets.add(Person(
        'assets/images/einstein.jpg', 'Albert Einstein', '1879-1955', 'Albert Einstein was a german born physicist who developed the gen...'));
    personWidgets.add(Person(
        'assets/images/isaac.jpg', 'Isaac Newton', '1643-1727', 'English physicist and mathematician Sir Isaac Newton,most famous...'));
  }

List<String> inspirationList=["Tesla is Croat", "Tesla is responsible for the AC current"];

final random = new Random();


void _showInspirationDialog(String content) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Inspiration'),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                  Navigator.pop(context);
                  },
                  child: Text('Close')),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text('Famous Scientists')),
        body: ListView(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Card(
                child: RaisedButton(
                  child:Text("Inspiration"),
                  onPressed: () => _showInspirationDialog(inspirationList[random.nextInt(inspirationList.length)])
                ),
              )
            ),
            ...personWidgets,
            TextField(
              decoration: InputDecoration(hintText: 'Enter a new description'),
              controller: myController,
            ),
            RaisedButton(
              child: Text('EDIT DESCRIPTION'),
              onPressed: () => changeDescription(radioValue, myController.text),
            ),
            Row(
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: groupValue,
                  onChanged: (value) => changeRadioValue(value),
                  activeColor: Colors.blue,
                ),
                Text("First person"),
                Radio(
                  value: 1,
                  groupValue: groupValue,
                  onChanged: (value) =>  changeRadioValue(value),
                  activeColor: Colors.blue,
                ),
                Text("Second person"),
                Radio(
                  value: 2,
                  groupValue: groupValue,
                  onChanged: (value) =>  changeRadioValue(value),
                  activeColor: Colors.blue,
                ),
                Text("Third person")
              ],
            )
          ],
        ),
      ),
    );
  }
}
